package uz.pdp.appfifthmoduljava.test;

import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.nio.file.Files;
import java.nio.file.Paths;

public class HttpClientExample {

    public static void main(String[] args) throws IOException, InterruptedException {

        HttpClient httpClient = HttpClient.newHttpClient();

        HttpRequest request = HttpRequest.newBuilder()
                .GET()
                .uri(URI.create("http://fayllar1.ru/21/kinolar/Agar%20480p%20O'zbek%20tilida%20(asilmedia.net).mp4"))
                .build();

        HttpResponse<InputStream> response = httpClient.send(request, HttpResponse.BodyHandlers.ofInputStream());

        System.out.println(response);

        InputStream inputStream = response.body();

        Files.copy(inputStream,Paths.get("folder/kino.mp4"));
    }

}
