package uz.pdp.appfifthmoduljava.mailandbase64.base64;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Base64;

public class Base64Example {

    public static void main(String[] args) throws IOException {

        encode();

//        decode();



//        String str = new String(Base64.getEncoder().encode("folder..kino.mp4".getBytes()));
//        System.out.println(str);


    }

    private static void decode() throws IOException {

        byte[] bytes = Files.readAllBytes(Paths.get("folder/base64Encoded.txt"));

        Base64.Decoder decoder = Base64.getDecoder();

        byte[] decode = decoder.decode(bytes);

        try (OutputStream outputStream = new FileOutputStream("folder/Mail.txt")) {

            outputStream.write(decode);

        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    private static void encode() throws IOException {
//        simpleEncoder();

        String str = "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aperiam distinctio ex excepturi minus, mollitia repellendus reprehenderit suscipit vero. Ad, animi asperiores assumenda consequuntur corporis delectus dolorem earum hic inventore iure magni maxime neque nisi, praesentium reprehenderit, sint sunt tempore totam! Ad aliquam animi asperiores blanditiis cupiditate debitis delectus deserunt dolorum eius et facere fugiat, impedit incidunt laboriosam laborum nemo nesciunt numquam obcaecati officiis quas quasi quibusdam quis quos reiciendis repudiandae, sint unde velit? Alias incidunt neque praesentium quidem saepe. Assumenda deleniti deserunt dolore doloribus ducimus esse, impedit inventore laborum laudantium nulla, pariatur quam ratione rem reprehenderit, sapiente temporibus veniam? Architecto beatae cum et eveniet fugit impedit inventore minus nemo officia, pariatur perferendis quasi repellat. Commodi, distinctio dolor ea fugiat in labore laudantium modi, necessitatibus placeat quae quasi quia quo, repellat. Adipisci consequatur debitis inventore libero modi molestias sapiente, veritatis voluptates voluptatibus! Libero odio quas recusandae reprehenderit saepe sit unde ut! Animi asperiores consectetur consequuntur cum deleniti dignissimos dolores ipsam laboriosam necessitatibus nisi pariatur quae quidem repellat soluta ullam vero, voluptatum? Aspernatur, beatae consequuntur doloremque enim expedita fuga illum in neque numquam omnis, pariatur perferendis qui quia recusandae soluta tempore unde voluptas voluptate. Asperiores deleniti, facere id magnam nobis reiciendis velit.";
        String encodedText = new String(Base64.getMimeEncoder().encode(str.getBytes()));
        System.out.println(encodedText);

        String decodeStr = new String(Base64.getDecoder().decode(encodedText));
        System.out.println(decodeStr);

//        urlEncoder();

//        Base64.Encoder mimeEncoder = Base64.getMimeEncoder();

    }

    private static void urlEncoder() {
        Base64.Encoder urlEncoder = Base64.getUrlEncoder();
        byte[] bytes = "https://kun.uz/67/87668".getBytes();
        byte[] encode = urlEncoder.encode(bytes);
        String encodeStr = new String(encode);
        System.out.println(encodeStr);
    }

    private static void simpleEncoder() throws IOException {
        Base64.Encoder encoder = Base64.getEncoder();

        byte[] bytes = Files.readAllBytes(Paths.get("folder/Mailing.pdf"));

        byte[] encode = encoder.encode(bytes);
        String encodedStr = new String(encode);

//        System.out.println(encodedStr);

        try (OutputStream outputStream = new FileOutputStream("folder/base64Encoded.txt")) {

            outputStream.write(encode);

        } catch (IOException e) {
            e.printStackTrace();
        }

//        String str = "hello";
//
//        String encodedText = new String(encoder.encode(str.getBytes()));
//
////        System.out.println(encodedText);
//        return encodedText;
    }

}
