package uz.pdp.appfifthmoduljava.mailandbase64.mailing;

import javax.mail.*;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Properties;

public class MailTest {

    public static void main(String[] args) throws FileNotFoundException {

        String to = "mehrojbek970599@gmail.com";

        String username = "c45b51d021d1b2";
        String password = "ca05c05e368234";
//        String username = "c45b51d021d1b2";
//        String username = "mehrojbek970510@gmail.com";
//        String password = "ahcg lrfv igcd qgac";

        Properties properties = getProperty();

        Session session = getSession(properties, username, password);

        //compose the message
        try {
            MimeMessage message = new MimeMessage(session);

            message.setFrom(new InternetAddress(username));

            message.addRecipient(Message.RecipientType.TO, new InternetAddress(to));

            FileInputStream inputStream = new FileInputStream("folder/Mailing.pdf");
            BodyPart bodyPart = new MimeBodyPart(inputStream);
            MimeMultipart mimeMultipart = new MimeMultipart(bodyPart);
//            message.setContent(mimeMultipart,"application/pdf");

            //title
            message.setSubject("Ping");
//            message.setText("Hello, this is example of sending email");
            message.setContent("<div style=\"text-align: center\">\n" +
                    "    <h1>Bu h2 tag</h1>\n" +
                    "    <h2>Bu h2 tag</h2>\n" +
                    "</div>\n" +
                    "<p style=\"color: red; text-align: center\">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Autem esse eum\n" +
                    "    excepturi nesciunt, odio perspiciatis porro quam veniam vitae voluptatum. Accusamus ad autem culpa illo inventore\n" +
                    "    labore molestiae? Consequatur facere illum ipsa, iusto natus vitae voluptatem! Doloribus, in, quos! Non odit porro\n" +
                    "    repudiandae vero voluptatum. Debitis ducimus error, iure laudantium nemo reprehenderit soluta veniam! Animi\n" +
                    "    assumenda, debitis distinctio ducimus expedita inventore nemo neque nihil, quo quod rem reprehenderit veritatis\n" +
                    "    voluptatem! Aliquam autem commodi deleniti enim et expedita, facilis id illo impedit incidunt inventore iure\n" +
                    "    laboriosam magnam nulla odio officiis praesentium quam quia repellat repellendus saepe sapiente sunt tenetur\n" +
                    "    voluptates voluptatum?</p>\n" +
                    "<div style=\"text-align: center\">\n" +
                    "    <img style=\"width: 500px\" src=\"https://www.w3schools.com/html/img_chania.jpg\">\n" +
                    "    <br>\n" +
                    "    <br>\n" +
                    "    <a href=\"https://kun.uz\">kunuz</a>\n" +
                    "</div>", "text/html");

            // Send message
            Transport.send(message);
            System.out.println("message sent successfully....");

        } catch (MessagingException mex) {
            mex.printStackTrace();
        }
    }

    private static Properties getProperty() {
        Properties properties = new Properties();
        properties.put("mail.smtp.host", "sandbox.smtp.mailtrap.io");
        properties.put("mail.smtp.port", "587");
        properties.put("mail.smtp.starttls.enable", "true");
        properties.put("mail.smtp.auth", "true");

//        properties.put ("mail.smtp.host", "smtp.gmail.com");
//        properties.put ("mail.smtp.port", "465");
//        properties.put("mail.smtp.ssl.enable", "true");
//        properties.put ("mail.smtp.auth", "true");

        return properties;
    }

    private static Session getSession(Properties properties, String username, String password) {
        return Session.getDefaultInstance(properties, new Authenticator() {
            @Override
            protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication(username, password);
            }
        });
    }



}


//        Properties properties = new Properties();
//        properties.put ("mail.smtp.host", "smtp.gmail.com");
//        properties.put ("mail.smtp.port", "465");
//        properties.put("mail.smtp.ssl.enable", "true");
//        properties.put ("mail.smtp.auth", "true");
//
//        String username = "john.lgd65@mail.com";
//        String password = "dbch";
//        Session session = getSession(properties, username, password);
//        Message message = new MimeMessage (session);
//        message.setSubject ("This is Subject For Test Message");
//        message.setText ("Body of mail here");
//        message.setFrom (new InternetAddress (username));
//        String recipient = "dev.jlkeesh@gmail.com";
//        message.setRecipient (Message.RecipientType.TO, new InternetAddress(recipient, Transport.send (message);
//        System.out.println ("Message Sent Successfully");
